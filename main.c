#include <stdio.h>

enum Language {
    EN,
    ES,
    FR
};

char* hello(enum Language lang) {
    switch (lang) {
        case ES: return "Hola";
        case FR: return "Bonjour";
        default: return "Hello";
    }
}

void greet(enum Language lang, char* person) {
    printf("%s, %s!\n", hello(lang), person);
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        printf("hello: 1 argument required\n");
        return 1;
    }

    greet(EN, argv[1]);
    return 0;
}

