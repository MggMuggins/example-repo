CC=gcc
CFLAGS=-I.
OBJ:=$(patsubst %.c,%.o,$(wildcard *.c))

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

a.out: $(OBJ)
	$(CC) -o a.out *.o

